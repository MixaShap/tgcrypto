import telebot
from telebot import types

from credentials import TELEGRAM_API_KEY

bot = telebot.TeleBot(TELEGRAM_API_KEY)
bot_link = 'https://t.me/mixashap_cryptobot'


@bot.message_handler(commands=['start'])
def start_message(message):
    print(f'User {message.from_user.id} aka {message.from_user.username} started conversation')
    bot.send_message(message.from_user.id, 'Hello, I am a CryptoRef-bot!')

    # Check if user has referrer
    if " " in message.text:
        referrer_candidate = message.text.split()[1]
        try:
            referrer_candidate = int(referrer_candidate)
            print(f'User {message.from_user.id} has referrer {referrer_candidate}')
        except ValueError:
            pass

    # Main menu
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    item1 = types.KeyboardButton("Получить реферальную ссылку")
    item2 = types.KeyboardButton("Other useless button")
    markup.add(item1)
    markup.add(item2)
    bot.send_message(message.chat.id, 'Выберите действие', reply_markup=markup)


@bot.message_handler(content_types=['text'])
def message_reply(message):
    if message.text == "Получить реферальную ссылку":
        bot.send_message(message.chat.id, f"{bot_link}?start={message.from_user.id}")

        # Ref menu
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        item1 = types.KeyboardButton("Мои рефералы")
        item2 = types.KeyboardButton("Мои финансы")
        item3 = types.KeyboardButton("Заказать выплату")
        item4 = types.KeyboardButton("На главную")
        markup.add(item1)
        markup.add(item2)
        markup.add(item3)
        markup.add(item4)
        bot.send_message(message.chat.id, 'Выберите действие', reply_markup=markup)

    elif message.text == "Мои рефералы":
        # TODO: get referrers from DB
        pass
    elif message.text == "Мои финансы":
        # Finances menu
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        item1 = types.KeyboardButton("Актуальный баланс")
        item2 = types.KeyboardButton("Отчет по средствам")
        item3 = types.KeyboardButton("На главную")
        markup.add(item1)
        markup.add(item2)
        markup.add(item3)
        bot.send_message(message.chat.id, 'Выберите действие', reply_markup=markup)
    elif message.text == "Заказать выплату":
        # TODO: payout menu
        pass
    elif message.text == "Актуальный баланс":
        # TODO: get balance from somewhere
        pass
    elif message.text == "Отчет по средствам":
        # TODO: get report from somewhere
        pass
    elif message.text == "На главную":
        # Main menu
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        item1 = types.KeyboardButton("Получить реферальную ссылку")
        item2 = types.KeyboardButton("Other useless button")
        markup.add(item1)
        markup.add(item2)
        bot.send_message(message.chat.id, 'Выберите действие', reply_markup=markup)
    elif message.text == "Your buttons":
        # Your logic here
        pass


bot.infinity_polling()
